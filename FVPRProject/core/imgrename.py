#!/usr/bin/python
# -*- coding: UTF-8 -*-
# ---------------------------------------------------------
# 功能说明：批量重命名图片名称，将图片统一命名为：img + 大类编号 + 小类编号 + .jpg
# 示例说明：img00010100001.jpg : 0001代表苹果大类；01代表富士苹果；0000x代表富士苹
#           果的数量.
# 作者: lkl
# 时间：2018-7-3
# ---------------------------------------------------------
import os


class BatchRename:
    def __init__(self):
        self.PATH = r"C:\Users\lkl\Desktop\temp"
        self.SUFFIX = [".jpg", ".png"]
        self.CURRENT_FRUIT_TYPE = "0001"
        self.CURRENT_FRUIT_KIND = "01"
        self.CURRENT_FRUIT_NUM_IN_TOTAL = 0

    # def rename_img(self):
    #     current_num = 00000
    #     for root, dirs, images in os.walk(self.PATH):
    #         for name in images:
    #             if name.endswith(self.SUFFIX[0]):
    #                 self.CURRENT_FRUIT_NUM_IN_TOTAL += 1
    #                 src = os.path.join(os.path.abspath(self.PATH), name)
    #                 dst = os.path.join(os.path.abspath(self.PATH),
    #                                    '' + "img" + self.CURRENT_FRUIT_TYPE
    #                                    + self.CURRENT_FRUIT_KIND
    #                                    + str(current_num)
    #                                    + self.SUFFIX[0])
    #                 try:
    #                     os.rename(src, dst)
    #                     print ('converting %s to %s ...' % (src, dst))
    #                     current_num += 1
    #                 except:
    #                     print("The file %s has already existed and does not need to be renamed" % src)
    #                     continue
    #         print ('There are %d images in current folder.' % self.CURRENT_FRUIT_NUM_IN_TOTAL)
    #         print ('There are %d images are renamed.' % current_num)

    def rename_img(self):
        for root, dirs, images in os.walk(self.PATH):
            for dir in dirs:
                print(dir)
                self.CURRENT_FRUIT_TYPE += 1
                target_src = os.path.join(os.path.abspath(self.PATH) + "\\" + dir)
                for subroot, subdirs, subimages in os.walk(target_src):
                    current_num = 1
                    for name in subimages:
                        # if name.endswith(self.SUFFIX[0]):
                        if name.endswith(".jpg" or ".jpeg" or ".png" or ".JPG"):
                            pass
                            self.CURRENT_FRUIT_NUM_IN_TOTAL += 1
                            src = os.path.join(os.path.abspath(target_src), name)
                            dst = os.path.join(os.path.abspath(target_src),
                                               '' + "img" + str(self.CURRENT_FRUIT_TYPE).zfill(4)
                                               + str(self.CURRENT_FRUIT_KIND).zfill(2)
                                               + str(current_num).zfill(5)
                                               + self.SUFFIX[0])
                            try:
                                os.rename(src, dst)
                                print ('converting %s to %s ...' % (src, dst))
                                current_num += 1
                            except:
                                print("The file %s has already existed and does not need to be renamed" % src)
                                continue
                    print ('There are %d images in current folder.' % self.CURRENT_FRUIT_NUM_IN_TOTAL)
                    print ('There are %s images are renamed.' % current_num)


if __name__ == '__main__':
    convert = BatchRename()
    convert.rename_img()
