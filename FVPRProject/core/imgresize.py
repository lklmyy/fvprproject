#!/usr/bin/python
# -*- coding: UTF-8 -*-

# ---------------------------------------------------------
# 功能说明：批量将图片尺寸统一为1000*1000
# 实现原理：
#   1、获取图像的长宽信息；
#   2、获取图像中心坐标；
#   3、选择ROI区域：中心与原图像重合，边长为原图像较边的长度
#   4、截取ROI；
#   5、使用resize()方法将ROI区域修改为1000*1000图像并保存
# 实现背景;在图片处理成1000*1000的尺寸有以下问题：
#          1、直接将图片长宽改为1000*1000的话图片会发生变形，但是背景前景不受影响；
#          2、如从原图中截取1000*1000的图片，有些种类可能只截取一部分（如芦笋、香蕉），
#             这种方法也能保证前景背景不受影响，但是只能截取部分果蔬信息；
#          3、如果对原图像进行等比例放缩的话，需要对图像较短一边的背景进行填充，这样不
#             会影响果蔬信息，但是背景会被改变。
# 注意事项：对于填充整幅图像的物体、细长条状物体不适用本方法。
#           拍照片的时候尽量把物体摆放在中央位置，隔一定的距离拍摄
# 作者: lkl
# 创建时间：2018-7-14
# 修改时间：2018-7-15
# ---------------------------------------------------------

import os
import cv2
import numpy as np


class ResizeImg(object):
    def __init__(self):
        self.file_dir_in = r"C:\Users\lkl\Desktop\temp"  # 读入文件路径
        self.file_dir_out = r"C:\Users\lkl\Desktop\dst"  # 读出文件路径
        self.count = 0  # 统计图片数量

    def batch_resize(self, dst_height=1000, dst_width=1000):
        for root, dirs, images in os.walk(self.file_dir_in):
            for dir in dirs:
                cur_count = 0
                # print(dir)
                target_src = os.path.join(os.path.abspath(self.file_dir_in) + "\\" + dir)
                dst_image_path = os.path.join(os.path.abspath(self.file_dir_out) + "\\" + dir)
                # print(dst_image_path)
                if not os.path.exists(dst_image_path):
                    os.makedirs(dst_image_path)
                for subroot, subdirs, subimages in os.walk(target_src):
                    current_num = 1
                    for name in subimages:
                    # for name in images:
                        if name.endswith('.jpg' or '.png' or 'JPG'):
                            # print(name)
                        # if name.endswith('JPG'):
                            src = os.path.join(os.path.abspath(target_src), name)
                            dst_path = os.path.join(os.path.abspath(dst_image_path), name)
                            # print(src)
                            image = cv2.imread(src)
                            croyImg = image
                            (srcheight, srcwidth) = croyImg.shape[:2]
                            # print((srcheight, srcwidth))
                            half_height = srcheight * 0.5
                            half_width = srcwidth * 0.5
                            # print((half_height, half_width))
                            if (srcheight, srcwidth) == (4160L, 3120L):  # (4160L, 3120L)代表图像尺寸，需要根据情况修改
                            # if (srcheight, srcwidth) == (4032L, 3024L):
                                pass  # 此处如果进行图片90度旋转可避免使用判断
                                up_edge = half_height - half_width
                                below_edge = half_height + half_width
                                left_edge = 0
                                right_edge = srcwidth
                                # 获取感兴趣区域:以中心为参照，截取shortest_edge*shortest_edge的图像
                                cropImg = croyImg[int(up_edge):int(below_edge), int(left_edge):int(right_edge)]
                                # 放缩为1000*1000的图像
                                dst = cv2.resize(cropImg, (dst_height, dst_width))
                                # 保证本地有路径，做相应修改
                                # cv2.imwrite(r'C:\Users\lkl\Desktop\dat\walnut_{:04d}.jpg'.format(self.count), dst)
                                cv2.imwrite(dst_path, dst)
                                print("Change original image (%d, %d) to (1000, 1000)" % (srcheight, srcwidth))
                            elif (srcheight, srcwidth) == (3120L, 4160L):
                            # elif (srcheight, srcwidth) == (3024L, 4032L):
                                up_edge = 0
                                below_edge = srcheight
                                left_edge = half_width - half_height
                                right_edge = half_width + half_height
                                # 获取感兴趣区域:以中心为参照，截取shortest_edge*shortest_edge的图像
                                cropImg = croyImg[int(up_edge):int(below_edge), int(left_edge):int(right_edge)]
                                # 放缩为1000*1000的图像
                                dst = cv2.resize(cropImg, (dst_height, dst_width))
                                # cv2.imwrite(r'C:\Users\lkl\Desktop\dat\walnut_{:04d}.jpg'.format(self.count), dst)
                                cv2.imwrite(dst_path, dst)
                                print("Change original image (%d, %d) to (1000, 1000)" % (srcheight, srcwidth))
                            else:
                                dst = cv2.resize(croyImg, (dst_height, dst_width))
                                # cv2.imwrite(r'C:\Users\lkl\Desktop\dat\walnut_{:04d}.jpg'.format(self.count), dst)
                                cv2.imwrite(dst_path, dst)
                                print("Change original image (%d, %d) to (1000, 1000)" % (srcheight, srcwidth))
                            self.count += 1
                            cur_count += 1
                    print("Change %d images in current dir" % cur_count)
        print("Change %d images in total" % self.count)


if __name__ == '__main__':
    resize_img = ResizeImg()
    resize_img.batch_resize()
