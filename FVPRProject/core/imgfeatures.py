#!/usr/bin/python2
# -*- coding: UTF-8 -*-

import math
import os
import sys
import numpy as np
import cv2
print 'cv version: ', cv2.__version__
np.set_printoptions(suppress=True)
"""
说明：提取纹理特征、SURF特征
"""


class GrayLevelCurMatrix(object):
    """
    # ---------------------------------------------------------
    # Instruction：基于灰度共生矩阵计算出来的统计量：
    #               1.角二阶矩（Angular Second Moment, ASM)
    #                 角二阶矩又称能量，是图像灰度分布均匀程度和纹理粗细的一个度量，反映了图像灰度分布均匀程度和纹理粗细度。
    #                 当图像纹理均一规则时，能量值较大；反之灰度共生矩阵的元素值相近，能量值较小。
    #               2.熵（Entropy, ENT)
    #                 熵度量了图像包含信息量的随机性，表现了图像的复杂程度。当共生矩阵中所有值均相等或者像素值表现出最大的随
    #                 机性时，熵最大。
    #               3.对比度
    #                 对比度反应了图像的清晰度和纹理的沟纹深浅。纹理越清晰反差越大对比度也就越大。
    #               4.反差分矩阵（Inverse Differential Moment, IDM)
    #                 反差分矩阵又称逆方差，反映了纹理的清晰程度和规则程度，纹理清晰、规律性较强、易于描述的，值较大。
    # Refer: Python： https://blog.csdn.net/kmsj0x00/article/details/79463376
    #                 https://blog.csdn.net/qq_23926575/article/details/80599323
    #                 https://blog.csdn.net/Eddy_zheng/article/details/78916009
    #        参考文献; https://blog.csdn.net/light_lj/article/details/26098815
    #        C++： https://blog.csdn.net/cv_yuippe/article/details/17096845
    # Auther: lkl
    # Time：2018-7-12
    """
    """
     获取灰度共生矩阵特征
    """
    def __init__(self):
        # 定义最大灰度级数当灰度级较大时，是一个庞大的矩阵。对于一般的灰度图，灰度级就有256，那么中就有个元素，如此庞大
        # 的矩阵会使后续的计算量增加，所以灰度共生矩阵一般要经过处理以减少灰度级数，比如通过分析纹理图像直方图，在不影响
        # 图像纹理质量的前提下，经过适当的变换压缩灰度级。
        self.gray_level = 16
        self.PATH = r"C:\Users\lkl\Desktop\temp\apple"
        self.SUFFIX = [".jpg", ".png"]
        self.CURRENT_FRUIT_TYPE = "0001"
        self.CURRENT_FRUIT_KIND = "01"
        self.CURRENT_FRUIT_NUM_IN_TOTAL = 0

    def max_gray_level(self, img):
        max_gray_level = 0
        (height, width) = img.shape
        # print(height, width)
        for y in range(height):
            for x in range(width):
                if img[y][x] > max_gray_level:
                    max_gray_level = img[y][x]
        return max_gray_level+1

    def glcm(self, inputimage, d_x, d_y):
        srcdata = inputimage.copy()
        ret = [[0.0 for i in range(self.gray_level)] for j in range(self.gray_level)]
        (height, width) = inputimage.shape

        max_gray_level = self.max_gray_level(inputimage)

        #  若灰度级数大于gray_level，则将图像的灰度级缩小至gray_level，减小灰度共生矩阵的大小
        if max_gray_level > self.gray_level:
            for j in range(height):
                for i in range(width):
                    srcdata[j][i] = srcdata[j][i]*self.gray_level / max_gray_level

        for j in range(height-d_y):
            for i in range(width-d_x):
                rows = srcdata[j][i]
                cols = srcdata[j + d_y][i+d_x]
                ret[rows][cols] += 1.0

        for i in range(self.gray_level):
            for j in range(self.gray_level):
                ret[i][j] /= float(height*width)

        return ret

    def feature_computer(self, p):
        contrast = 0.0  # 对比度，值越大，纹理越清晰
        entropy = 0.0  # 熵（Entropy, ENT)，值越大，纹理较多
        asm = 0.0  # 角二阶矩（Angular Second Moment, ASM),即能量特征 ，值越大，纹理越粗，反之越细
        idm = 0.0  # 反差分矩阵（Inverse Differential Moment, IDM),值越大，图像纹理不同区域间变化小，局部非常均匀
        pass
        correlation = 0.0  # 自相关性反应了图像纹理的一致性
        # 惯性矩
        # correlation += (i * j * m - ui * uj) / sqrt(si_2) * sqrt(sj_2);

        for i in range(self.gray_level):
            for j in range(self.gray_level):
                contrast += (i-j)*(i-j)*p[i][j]
                asm += p[i][j]*p[i][j]
                idm += p[i][j]/(1+(i-j)*(i-j))
                if p[i][j] > 0.0:
                    entropy += p[i][j]*math.log(p[i][j])
        return asm, contrast, -entropy, idm

    def get_glcm_features(self):
        try:
            glcmfeaturevalues = []
            img = cv2.imread(r"C:\Users\lkl\Desktop\temp\apple\img00010100001.jpg")
            img_shape = img.shape
            img = cv2.resize(img, (img_shape[1] / 2, img_shape[0] / 2), interpolation=cv2.INTER_CUBIC)
            img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            glcm_0 = self.glcm(img_gray, 1, 0)
            asm, con, eng, idm = self.feature_computer(glcm_0)
            # print(asm, con, eng, idm)
            glcmfeaturevalues.append(round(asm, 3))
            glcmfeaturevalues.append(round(con, 3))
            glcmfeaturevalues.append(round(eng, 3))
            glcmfeaturevalues.append(round(idm, 3))
            # print(glcmfeaturevalues)
            print ('苹果图像的纹理特征(能量，对比度，熵，逆方差)值为别为：%.3f,%.3f,%.3f,%.3f'
                   % (glcmfeaturevalues[0], glcmfeaturevalues[1], glcmfeaturevalues[2], glcmfeaturevalues[3]))
            return glcmfeaturevalues
        except Exception as e:
            print ('Image read error')
            print(e)

    def batch_get_glcm_features(self):
        current_num = 00001
        glcmfeaturevalues = []
        try:
            for root, dirs, images in os.walk(self.PATH):
                for name in images:
                    if name.endswith(self.SUFFIX[0]):
                        self.CURRENT_FRUIT_NUM_IN_TOTAL += 1
                        src = os.path.join(os.path.abspath(self.PATH), name)
                        # print(src)
                        img = cv2.imread(src)
                        img_shape = img.shape
                        img = cv2.resize(img, (img_shape[1] / 2, img_shape[0] / 2), interpolation=cv2.INTER_CUBIC)
                        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

                        glcm_0 = self.glcm(img_gray, 1, 0)
                        asm, con, eng, idm = self.feature_computer(glcm_0)
                        # print(asm, con, eng, idm)
                        glcmfeaturevalues.append(round(asm, 3))
                        glcmfeaturevalues.append(round(con, 3))
                        glcmfeaturevalues.append(round(eng, 3))
                        glcmfeaturevalues.append(round(idm, 3))
                        # print(glcmfeaturevalues)
                        print ('第 %d 张苹果图像的灰度共生矩阵特征(能量，对比度，熵，逆方差)值为别为：%.3f,%.3f,%.3f,%.3f'
                               % (current_num, glcmfeaturevalues[-4], glcmfeaturevalues[-3], glcmfeaturevalues[-2],
                                  glcmfeaturevalues[-1],))
                        current_num += 1
                        # return glcmfeaturevalues
        except Exception as e:
            # print ('Image read error')
            print(e)


class GraysLGradCoccurMatrix(object):
    """
    获取灰度梯度共生矩阵特征
    """
    def __init__(self):
        pass

    def glgcm_features(self, mat):
        """灰度梯度共生矩阵(Gray Level-GradientCo-occurrence Matrix)将图梯度信息加入到灰度共生矩阵中，综合利用图像的灰度
        与梯度信息，效果更好。图像的梯度信息一般通过梯度算子(也称边缘检测算子)提取，如sobel、canny、reborts等。基于规范
        化后的灰度梯度共生矩阵，可以计算一系列的二次统计特征。如下为15个常用的数字特征：小梯度优势、大梯度优势、灰度分布
        不均匀性、梯度分布不均匀性、能量、灰度平均、梯度平均、灰度均方差、梯度均方差、相关、灰度熵、梯度熵、混合熵、惯性
        、逆差矩。根据灰度梯度共生矩阵计算纹理特征量，包括小梯度优势，大梯度优势，灰度分布不均匀性，梯度分布不均匀性，
        能量，灰度平均，梯度平均，灰度方差，梯度方差，相关，灰度熵，梯度熵，混合熵，惯性，逆差矩
        """
        sum_mat = mat.sum()
        small_grads_dominance = big_grads_dominance = gray_asymmetry = grads_asymmetry = energy = gray_mean =\
            grads_mean = 0
        gray_variance = grads_variance = corelation = gray_entropy = grads_entropy = entropy = inertia = \
            differ_moment = 0
        for i in range(mat.shape[0]):
            gray_variance_temp = 0
            for j in range(mat.shape[1]):
                small_grads_dominance += mat[i][j] / ((j + 1) ** 2)
                big_grads_dominance += mat[i][j] * j ** 2
                energy += mat[i][j] ** 2
                if mat[i].sum() != 0:
                    gray_entropy -= mat[i][j] * np.log(mat[i].sum())
                if mat[:, j].sum() != 0:
                    grads_entropy -= mat[i][j] * np.log(mat[:, j].sum())
                if mat[i][j] != 0:
                    entropy -= mat[i][j] * np.log(mat[i][j])
                    inertia += (i - j) ** 2 * np.log(mat[i][j])
                differ_moment += mat[i][j] / (1 + (i - j) ** 2)
                gray_variance_temp += mat[i][j] ** 0.5

            gray_asymmetry += mat[i].sum() ** 2
            gray_mean += i * mat[i].sum() ** 2
            gray_variance += (i - gray_mean) ** 2 * gray_variance_temp
        for j in range(mat.shape[1]):
            grads_variance_temp = 0
            for i in range(mat.shape[0]):
                grads_variance_temp += mat[i][j] ** 0.5
            grads_asymmetry += mat[:, j].sum() ** 2
            grads_mean += j * mat[:, j].sum() ** 2
            grads_variance += (j - grads_mean) ** 2 * grads_variance_temp
        small_grads_dominance /= sum_mat
        big_grads_dominance /= sum_mat
        gray_asymmetry /= sum_mat
        grads_asymmetry /= sum_mat
        gray_variance = gray_variance ** 0.5
        grads_variance = grads_variance ** 0.5
        for i in range(mat.shape[0]):
            for j in range(mat.shape[1]):
                corelation += (i - gray_mean) * (j - grads_mean) * mat[i][j]
        glgcm_features = [small_grads_dominance, big_grads_dominance, gray_asymmetry, grads_asymmetry, energy,
                          gray_mean, grads_mean, gray_variance, grads_variance, corelation, gray_entropy, grads_entropy,
                          entropy, inertia, differ_moment]
        return np.round(glgcm_features, 4)

    def get_glgcm(self, img_gray, ngrad=16, ngray=16):
        """
         Gray Level-Gradient Co-occurrence Matrix,取归一化后的灰度值、梯度值分别为16、16
        """
        # 利用sobel算子分别计算x-y方向上的梯度值
        gsx = cv2.Sobel(img_gray, cv2.CV_64F, 1, 0, ksize=3)
        gsy = cv2.Sobel(img_gray, cv2.CV_64F, 0, 1, ksize=3)
        height, width = img_gray.shape
        grad = (gsx ** 2 + gsy ** 2) ** 0.5  # 计算梯度值
        cv2.imshow("grad", grad)
        grad = np.asarray(1.0 * grad * (ngrad-1) / grad.max(), dtype=np.int16)

        gray = np.asarray(1.0 * img_gray * (ngray-1) / img_gray.max(), dtype=np.int16)  # 0-255变换为0-15
        cv2.imshow("gray", gray)
        cv2.waitKey(5)
        gray_grad = np.zeros([ngray + 1, ngrad + 1])  # 灰度梯度共生矩阵
        for i in range(height):
            for j in range(width):
                gray_value = gray[i][j]
                grad_value = grad[i][j]
                gray_grad[gray_value][grad_value] += 1
        gray_grad = 1.0 * gray_grad / (height * width)  # 归一化灰度梯度矩阵，减少计算量
        # print(gray_grad)
        glgcm_features = self.glgcm_features(gray_grad)
        return glgcm_features


class SurfFeature(object):
    """
    # ---------------------------------------------------------
    # Instruction：Python利用opencv提取surf特征并保存。代码计算images_folder文件夹下的所有图片的SURF特征，然后将图片与特征向
    #   量其保存到指定文件夹
    # 算法背景介绍：
    #   Lowe于2000年提出了SIFT算法，并于2004年加以完善和改进，SIFT特征对图像旋转、平移、缩放、亮度变化能够保持良好的不变性，且
    #   其独特性好，信息量较为丰富，得到了广泛的应用，但其提取计算量较大，效率较低，因此Bay等人提出了SURF算法，在保证特征点数
    #   量的情况下，提高了效率。SURF算法首先构建Hessian矩阵，然后构建尺度空间(SIFT算法则使用DOG)，其在构建图像金字塔时原始图像
    #   大小保持不变，只改变滤波器大小，然后精确定位特征点并确定其主方向，最后生成特征点描述子。此外，SURF算法得到的特征向量维
    #   度为64，而SIFT得到的是128维向量。
    #  Environment: Python2 + OpenCV3.1.0
    # Refer: Python：https://blog.csdn.net/qq_23926575/article/details/80139234
    # Auther: lkl
    # Time：2018-7-12
    """
    def __init__(self):
        self.path = sys.path[0] + os.sep

    def surf_feature_extract(self, images_folder, draw_folder):
        """
        每幅图像中检测出来的关键点个数不同；SURF算法根据这些关键点计算出来的关键点相对应的特征向量或者说特征描述子的向量
        维数为64维；也就是说，使用一个64维的特征向量来描述图像中的一个关键点，n个关键点就代表n个特征向量；另外需要说
        明的一点，我们讲计算出来的特征向量是存储在Mat矩阵中的，在这个矩阵中，每一行代表一个关键点所对应的特征向量；
        所以这个Mat的size()，即cols*rows==64*n;如果使用的是两张完全一样的图片进行关键点的检测，特征向量的计算，
        特征向量的匹配，所以两幅图像中的关键点是一一对应的，匹配的关键点之间的距离也为0，即两个特征向量是完全匹配，完全
        一样的。
        """
        featuresum = 0
        for filename in os.listdir(images_folder):
            if '.jpg' in filename:
                filepath = images_folder + filename
                drawpath = draw_folder + filename
            else:
                continue
            img = cv2.imread(filepath)
            filename = filepath.split(os.sep)[-1].split('.')[0]
            gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            # set Hessian threshold
            detector = cv2.xfeatures2d.SURF_create(2000)
            # find keypoints and descriptors directly
            kps, des = detector.detectAndCompute(gray, None)
            drawedimg = cv2.drawKeypoints(image=img, outImage=img, keypoints=kps,
                                    flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS, color=(255, 0, 0))
            feature_name = images_folder + 'features%s%s.feature' % (os.sep, filename)
            try:
                feature_folder = "out_surf_feature.txt"
                np.savetxt(feature_folder, des, fmt='%.5e')  # 保存特征向量
                feature = np.loadtxt(feature_folder)  # 加载特征向量
                print(feature)
                cv2.imwrite(drawpath, drawedimg)  # 保存绘制了SURF特征的图片
            except:
                continue
            featuresum += len(kps)
        print featuresum

    def get_feature(self):
        images_folder = self.path + 'image' + os.sep + 'walnut' + os.sep
        draw_folder = self.path + 'results' + os.sep + 'walnutdrawImages' + os.sep
        self.surf_feature_extract(images_folder, draw_folder)

    def orb_feature(self):
        """
        https://blog.csdn.net/tengfei461807914/article/details/79693216
        :return:
        """
        featuresum = 0
        img = cv2.imread(r'C:\Users\lkl\Desktop\temp\apple\img00010100001.jpg', 0)
        orb = cv2.ORB_create()
        kp = orb.detect(img, None)
        kp, des = orb.compute(img, kp)
        img = cv2.drawKeypoints(img, kp, img, color=(0, 255, 0), flags=0)
        cv2.imshow('p', img)
        try:
            feature_folder = "out_surf_feature.txt"
            np.savetxt(feature_folder, des, fmt='%.5e')  # 保存特征向量
            feature = np.loadtxt(feature_folder)  # 加载特征向量
            print(feature)
            # cv2.imwrite(drawpath, drawedimg)  # 保存绘制了SURF特征的图片
        except Exception as e:
            print(e)
        featuresum += len(kp)
        print featuresum
        cv2.waitKey()


class ColorFeature(object):
    """
    提取图像颜色特征
    """
    def __init__(self):
        fp = r"C:\Users\lkl\Desktop\temp\apple\img00010100001.jpg"
        self.img = cv2.imread(fp)
        cv2.imshow("img", self.img)
        hsv = cv2.cvtColor(self.img, cv2.COLOR_BGR2HSV)  # HSV空间
        cv2.imshow("img1", hsv)
        cv2.waitKey()


if __name__ == '__main__':

    # 灰度共生矩阵
    # glcm = GrayLevelCurMatrix()
    #
    # glcm.get_glcm_features()
    # glcm.batch_get_glcm_features()

    # 灰度梯度共生矩阵
    # glgcm = GraysLGradCoccurMatrix()
    # fp = r"C:\Users\lkl\Desktop\temp\apple\img00010100001.jpg"
    # img = cv2.imread(fp)
    # cv2.imshow("img", img)
    # img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # glgcm_features = glgcm.get_glgcm(img_gray, 15, 15)
    # print glgcm_features

    # 提取SURF特征
    surffeature = SurfFeature()
    # surffeature.get_feature()
    surffeature.orb_feature()

    # color_feature = ColorFeature()
